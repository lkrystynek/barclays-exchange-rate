import Vue from 'vue'

export default {

fetchData({state, commit}, date) {
    commit('CHANGE_LOADING_DATA', true)
    
    return Vue.axios.get('denni_kurz.txt', {
      params: {
        date: date
      }, // end params
    })
    .then( response => {
      let items = response.data.split('\n')
      
      // remove unwanted data
      items.shift() 
      
      // format headers
      let headers = items.shift().split('|')
      for(let h in headers) {
        h == 0 ? headers[h] = { text: headers[h], align: 'center', value: 'head' + h } : headers[h] = { text: headers[h], align: 'center', sortable: false, value: 'head' + h }
        commit('CHANGE_DOWNLOADED_HEADER', headers)
      }

      // format data
      for(let i in items) {
        let itemsArr = items[i].split('|')
        let tempObj = {value: false}
        for (let item in itemsArr) {
          tempObj['head' + item] = itemsArr[item]
        }
        items[i] = tempObj
      }
  
      commit('CHANGE_DOWNLOADED_HEADER', headers)
      commit('CHANGE_DOWNLOADED_DATA', items)
      commit('CHANGE_LOADING_DATA', false)
    })
    .catch(err => {
      console.log(err)
      commit('CHANGE_LOADING_DATA', false)
    })
  } 

} // end export