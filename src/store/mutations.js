export default {
  CHANGE_LOADING_DATA(state, value) {
    // value: boolean
    state.loadingData = value
  },
  
  CHANGE_DOWNLOADED_HEADER(state, arr) {
    // arr: array
    state.downloadedHeader = arr
  },

  CHANGE_DOWNLOADED_DATA(state, arr) {
    // arr: arr
    state.downloadedData = arr
  }
}
