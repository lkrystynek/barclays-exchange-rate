import Vue from 'vue'
import App from './App'
import router from './router'
import VueEvents from 'vue-events'
import VueAxios from 'vue-axios'
import axios from 'axios'
import '../node_modules/vuetify/dist/vuetify.min.css'
import { store } from './store/index'
import Vuetify from 'vuetify'

Vue.config.productionTip = false

Vue.use(Vuetify, {
  theme: {
    primary: '#00a5dd',
    secondary: '#F7AD00',
    accent: '#8c9eff',
    error: '#b71c1c'
  }
})

Vue.use(VueAxios, axios)
Vue.use(VueEvents)

Vue.API_SERVER = Vue.prototype.API_SERVER = process.env.API_SERVER
Vue.axios.defaults.baseURL = '/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/'
Vue.axios.defaults.responseType = 'plain/text'
Vue.axios.defaults.headers.Accept = 'application/json'

/* eslint-disable no-new */
new Vue({
  router,
  el: '#app',
  store: store,
  render: h => h(App)
})
